﻿var st = st || {};

st.AppUser = Backbone.Model.extend({
    className: 'AppUser',

    defaults: {
        username: null
    },

    initialize: function () {
        _.bindAll(this, 'logIn', 'logOut', 'signUp', 'isLoggedIn');

        var parseUser = Parse.User.current();
        if (parseUser) {
            var username = parseUser.get('username');
            this.set('username', username);
        }
    },

    logIn: function (username, password, successCallback, errorCallback) {
        var self = this;
        Parse.User.logIn(username, password, {
            success: function (user) {
                self.set('username', username);
                if (successCallback) { successCallback(user); }
                self.trigger('logIn');
            },

            error: function (user, error) {
                if (errorCallback) { errorCallback(user, error); }
            }
        });
    },

    logOut: function () {
        Parse.User.logOut();
        this.set('username', null);
        this.trigger('logOut');
    },

    signUp: function (username, password, successCallback, errorCallback) {
        var self = this;
        Parse.User.signUp(username, password, { ACL: new Parse.ACL() }, {
            success: function (user) {
                self.set('username', username);
                if (successCallback) { successCallback(user); }
                self.trigger('signUp');
            },

            error: function (user, error) {
                if (errorCallback) { errorCallback(user, error); }
            }
        });
    },

    isLoggedIn: function () {
        if (Parse.User.current()) {
            return true;
        } else {
            return false;
        }
    },
});