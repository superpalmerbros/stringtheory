﻿var st = st || {};

st.PracticeSession = Backbone.Model.extend({
    className: 'PracticeSession',

    defaults: {
        username: null,
        start: new Date(new Date().toDateString()),
        duration: 300000,
        description: '',
    },

    initialize: function( ) {
        _.bindAll(this, 'get', 'set', 'increment', 'decrement', 'save', 'validate');
    },
    
    get: function(attr){
        var value = Backbone.Model.prototype.get.call(this, attr);
        return value;
    },

    set: function (key, val, options) {
        options = options || {};
        if (options.validate !== false) {
            options.validate = true;
        }
        return Backbone.Model.prototype.set.call(this, key, val, options);
    },

    increment: function (attr, val) {
        var prev = this.get(attr);
        var update = prev + val;
        this.set(attr, update);
    },

    decrement: function (attr, val) {
        var prev = this.get(attr);
        var update = prev - val;
        if (update < 0) {
            update = 0;
        }
        this.set(attr, update);
    },

    save: function (key, val, options) {
        var self = this;

        if (key == null) {
            options = val;
        } else if (typeof key == 'object') {
            this.set(key);
            options = val;
        } else {
            this.set(key, val);
        }

        var attrs = this.attributes;

        //modify options to pass success callback
        options = options || {};
        options.validate = true;
        //cache the original success callback
        var origSuccess = options.success;
        //trigger the original success callback
        options.success = function (model, response, options) {
            self.set('id', model.id);
            if (origSuccess) {
                origSuccess(self, model, options);
            }
        };

        var storage = new st.ParseStorage();        
        var value = storage.savePracticeSession(attrs, options);
        return value;        
    },

    validate: function (attributes, options) {
        var start = attributes['start'];
        var duration = attributes['duration'];
        var durationExists = !!duration || duration === 0;
        var finish = attributes['finish'];
        
        if (!start && !durationExists && !finish) {
            //if they are all null or undefined the model is valid
            return;
        }else if (!start && !durationExists && finish) {
            start = new Date(finish.getTime());
            duration = 0;
        } else if (!start && durationExists && !finish) {
            //no coercion
            return;
        }else if (!start && durationExists && finish) {
            start = new Date(finish.getTime() - duration);
        } else if (start && !durationExists && !finish) {
            //no coercion
            return;
        }else if (start && !durationExists && finish) {
            duration = finish.getTime() - start.getTime();
        }else if (start && durationExists && !finish) {
            finish = new Date(start.getTime() + duration);
        }else if (start && durationExists && finish) {
            //force consistency with finish being the dependent variable
            var validFinish = new Date(start.getTime() + duration);
            if (finish.getTime() == validFinish.getTime()) {
                return;
            } else {
                var oldStart = this.get('start');
                var oldDuration = this.get('duration');
                var oldFinish = this.get('finish');
                if (duration != oldDuration) {
                    finish = validFinish;
                } else if (!oldFinish || oldFinish.getTime() != finish.getTime()) {
                    duration = finish.getTime() - start.getTime();
                } else if (oldStart.getTime() != start.getTime()) {
                    if (start < finish) {
                        duration = finish.getTime() - start.getTime();
                    } else {
                        duration = 0;
                        finish = new Date(start.getTime());
                    }
                }
            }
        }
        
        Backbone.Model.prototype.set.call(
            this, 
            {
                start: start,
                duration: duration,
                finish: finish
            }, 
            options
        );
        
        return;
    },

});

st.PracticeSessionCollection = Backbone.Collection.extend({
    model: st.PracticeSession,
    
    initialize: function( ) {
        _.bindAll(this, 'fetchByUsername');
    },

    fetchByUsername: function (username, options) {
        var self = this;
        //initialize options if they are not passed
        options = options || {};
        //cache the original success callback
        var origSuccess = options.success;
        //reset the collection, and then trigger the original success callback
        options.success = function (collection, response, options) {
            self.reset(collection);
            if (origSuccess) {
                origSuccess(collection, response, options);
            }
        };

        var storage = new st.ParseStorage();
        storage.getPracticeSessionsByUsername(username, options);
    },
});