﻿var st = st || {};

Parse.$ = jQuery;

Parse.initialize('b9MkKUwiHtI3rTUEuvsLKFr2RQ7Lg7mnec7drfTh', 'gwUspVNCkzRsWhKyWx988X3ZhA1VxREdmhdRvI3L');

st.ParsePracticeSession = Parse.Object.extend({
    className: 'PracticeSession',
});

st.ParseStorage = function () {
    return {
        savePracticeSession: function (session, options) {
            var persister = new st.ParsePracticeSession();
            var results = persister.save(
                {
                    username: session.username,
                    start: session.start,
                    duration: session.duration,
                    description: session.description,
                },
                options
            );

            return results;
        },

        getPracticeSessionsByUsername: function (username, options) {
            options = options || {};
            var origSuccess = options.success;
            options.success = function (response) {
                if (origSuccess) {
                    var results = [];
                    response.forEach(function (element, index, list) {
                        var eAttrs = element.attributes;
                        eAttrs.id = element.id;
                        results.push(eAttrs);
                    });

                    origSuccess(results);
                }
            };

            var sessions = null;
            var query = new Parse.Query(st.ParsePracticeSession);
            query.equalTo('username', username);
            query.descending('start');
            query.find(options);
        },
    };
};