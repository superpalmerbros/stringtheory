﻿var st = st || {};

st.PracticeSessionsView = Backbone.View.extend({

    el: '#practice-sessions',

    initialize: function () {
        _.bindAll(this, 'render');

        var username = this.model.get('username');

        this.currentSession = new st.PracticeSession();
        this.currentSession.set('username', username);

        this.practiceSessions = new st.PracticeSessionCollection();
        this.practiceSessions.bind('all', this.render);
        this.practiceSessions.fetchByUsername(username);

        this.previousSessionsView = new st.PreviousSessionsView({ model: this.practiceSessions });
    },

    render: function () {
        var current = $('#practice-sessions div#current');
        var currentView = new st.CurrentSessionView({ model: this.currentSession });
        current.html(currentView.render().el);
    },
});