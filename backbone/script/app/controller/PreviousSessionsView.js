﻿var st = st || {};

st.PreviousSessionsView = Backbone.View.extend({

    el: '#history',

    template: _.template(spb.util.loadTemplate('#previous-sessions-template')),

    initialize: function () {
        _.bindAll(this, 'render');
        
        this.model.bind('all', this.render);
    },

    render: function () {
        var rendered = this.template();
        $(this.el).html(rendered);

        var itemContainer = $('#history div#items');
        itemContainer.html('');
        this.model.each(function (item) {
            var view = new st.PreviousSessionView({ model: item });
            itemContainer.append(view.render().el);
        });

        return this;
    },
});