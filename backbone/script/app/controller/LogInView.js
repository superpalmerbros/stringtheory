﻿var st = st || {};

st.LogInView = Backbone.View.extend({

    el: '#user-login',

    template: _.template(spb.util.loadTemplate('#login-template')),

    events: {
        'click input#log-in-button': 'logIn',
        'click input#sign-up-button': 'signUp'
    },

    initialize: function () {
        _.bindAll(this, 'render', 'logIn', 'signUp', 'logOut');
        this.render();
    },


    render: function () {
        $(this.el).html(this.template());
        this.delegateEvents();
    },

    logIn: function (e) {
        var self = this;
        var username = this.$('#login-username').val();
        var password = this.$('#login-password').val();

        this.$('.login-form button').attr('disabled', 'disabled');

        this.model.logIn(username, password,
            function (user) {
                self.undelegateEvents();
                delete self;
            },
            function (user, error) {
                self.$('.login-form .error').html('Invalid username or password. Please try again.').show();
                self.$('.login-form button').removeAttr('disabled');
            }
        );

        return false;
    },

    signUp: function (e) {
        var self = this;
        var username = this.$('#signup-username').val();
        var password = this.$('#signup-password').val();

        this.model.signUp(username, password,
            function (user) {
                self.undelegateEvents();
                delete self;
            },
            function (user, error) {
                self.$('.signup-form .error').html(error.message).show();
                self.$('.signup-form button').removeAttr('disabled');
            }
        );

        this.$('.signup-form button').attr('disabled', 'disabled');

        return false;
    },

    logOut: function (e) {
        this.model.logOut();
    },
});