﻿var st = st || {};

st.CurrentSessionView = Backbone.View.extend({

    template: _.template(spb.util.loadTemplate('#current-session-template')),

    events: {
        'change #current-description': 'viewDescriptionChanged',
        'change #current-duration': 'viewDurationChanged',
        'change #current-date': 'viewDateChanged',
        'change #current-start': 'viewStartChanged',
        'change #current-finish': 'viewFinishChanged',
        'click #save-current-session': 'saveSession',
        'click #toggle-stopwatch': 'toggleStopwatch',
        'click .close': 'closewin',
        'click #txlog': 'openlog',
        'click .minus-small': 'minussmall',
        'click .minus-big': 'minusbig',
        'click .plus-small': 'plussmall',
        'click .plus-big': 'plusbig',
    },

    viewModel: { stopwatchEngaged: false },

    initialize: function () {
        _.bindAll(this, 'render', 'viewDescriptionChanged', 'viewDurationChanged', 'viewDateChanged', 'viewStartChanged', 'viewFinishChanged', 'saveSession', 'toggleStopwatch', 'closewin', 'openlog');
        this.model.set('stopwatchEngaged', false);
        this.model.bind('change', this.render);
    },

    render: function () {
        var json = this.model.toJSON();
        var rendered = this.template(json);
        $(this.el).html(rendered);
        return this;
    },

    viewDescriptionChanged: function (e) {
        this.model.set('description', $('#current-description').val());
    },

    viewDurationChanged: function (e) {

        this.model.set('duration', $('#current-duration').val());
    },

    viewDateChanged: function (e) {
        this.model.set('date', new Date($('#current-date').val()));
    },

    viewStartChanged: function (e) {
        this.model.set('start', new Date($('#current-start').val()));
    },

    viewFinishChanged: function (e) {
        this.model.set('finish', new Date($('#current-finish').val()));
        //TODO: Does this actually do anything meaningful???
        $("#practice-session-current").css("color", "red");
    },

    saveSession: function () {
        this.model.save(null, {
            success: function (model, response, options) {
                alert('saved');
            },
            error: function (error) {
                alert('failed: ' + error);
            }
        });
    },

    toggleStopwatch: function () {
        $toggler = $('#toggle-stopwatch');
        if (this.model.get('stopwatchEngaged') == false) {
            this.model.set('stopwatchEngaged', true);
            $toggler.removeClass('watch-stopped').addClass('watch-started').html('Stop');
            this.model.set('start', new Date());
        } else {
            this.model.set('stopwatchEngaged', false);
            $toggler.removeClass('watch-started').addClass('watch-stopped').html('Go');
            this.model.set('finish', new Date());
            //TODO: Does this actually do anything meaningful???
            if ($("#current-description").html() != '') {
                $(".button").css("display", "block");
            }
        }
    },

    minussmall: function () {
        this.model.decrement('duration', 60000);
    },

    minusbig: function () {
        this.model.decrement('duration', 600000);
    },

    plussmall: function () {
        this.model.increment('duration', 60000);
    },

    plusbig: function () {
        this.model.increment('duration', 600000);
    },

    closewin: function () {
        this.trigger('current-session.close');
    },

    openlog: function () {
        this.trigger('current-session.open');
    },

});