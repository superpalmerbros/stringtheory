﻿var st = st || {};

st.PreviousSessionView = Backbone.View.extend({

    template: _.template(spb.util.loadTemplate('#previous-session-template')),

    initialize: function () {
        _.bindAll(this, 'render');
        this.model.bind('change', this.render);
    },

    render: function () {
        $(this.el).html(this.template(this.model.toJSON()));
        return this;
    },
});