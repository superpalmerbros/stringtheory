﻿var st = st || {};

// The main view for the app
st.AppView = Backbone.View.extend({

    // Instead of generating a new element, bind to the existing skeleton of
    // the App already present in the HTML.
    el: '#string-theory-app',

    user: null,
    loginView: null,
    practiceView: null,


    initialize: function () {
        _.bindAll(this, 'render', 'logIn', 'logOut');

        this.user = new st.AppUser();

        this.listenTo(this.user, 'logIn', this.logIn);
        this.listenTo(this.user, 'signUp', this.logIn);
        this.listenTo(this.user, 'logOut', this.logOut);

        this.render();
    },

    render: function () {
        if (this.user.isLoggedIn()) {
            this.practiceView = new st.PracticeSessionsView({ model: this.user });
        } else {
            this.loginView = new st.LogInView({ model: this.user });
        }
    },

    logIn: function () {
        this.loginView.remove();
        this.practiceView = new st.PracticeSessionsView({ model: this.user });
    },

    logOut: function () {
        this.practiceView.remove();
        this.loginView = new st.LogInView({ model: this.user });
    },

});