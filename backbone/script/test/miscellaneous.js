﻿module('SuperPalmerBros utility');
test('utility exists', function () {
    ok(spb.util, 'spb.util exists')
});

module('AppView')
test('either login or practice view exists', function () {
    ok(st.app.loginView || st.app.practiceView, 'st.app.loginView or st.app.practiceView is defined');
});
