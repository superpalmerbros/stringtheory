﻿module('PracticeSession');

test('PracticeSession exists', function () {
    ok(st.PracticeSession, 'st.PracticeSession is defined');
});

test('PracticeSession instance has .get function', function () {
    var instance = new st.PracticeSession();
    ok(instance.get, 'instance.get is defined');
});

test('PracticeSession instance has .set function', function () {
    var instance = new st.PracticeSession();
    ok(instance.set, 'instance.set is defined');
});

test('.get returns value that was .set', function () {
    var instance = new st.PracticeSession();
    var data = 'asdfsdjkghdsfuhgeiorutqwer';
    instance.set('data', data);
    var retreived = instance.get('data');
    equal(data, retreived, 'set value: ' + data + ', get value: ' + retreived);
});

test('.set triggers general change event', function () {
    var instance = new st.PracticeSession();
    var eventTriggered = false;
    var onTrigger = function () { eventTriggered = true; };
    instance.on('change', onTrigger, this);
    var data = 'asdfsdjkghdsfuhgeiorutqwer';
    instance.set('data', data);

    ok(eventTriggered, 'set triggered callback');
});

test('.set triggers attribute specific change event', function () {
    var instance = new st.PracticeSession();
    var eventTriggered = false;
    var onTrigger = function () { eventTriggered = true; };
    instance.on('change:data', onTrigger, this);
    var data = 'asdfsdjkghdsfuhgeiorutqwer';
    instance.set('data', data);

    ok(eventTriggered, 'set triggered specific callback');
});

test('username .get value equals last username .set value', function () {
    var instance = new st.PracticeSession();
    var original = 'newname';
    instance.set('username', original);
    var retrieved = instance.get('username');
    equal(original, retrieved, '.set username value equals retrieved username value');
});

test('description .get value equals last description .set value', function () {
    var instance = new st.PracticeSession();
    var original = 'newname';
    instance.set('description', original);
    var retrieved = instance.get('description');
    equal(original, retrieved, '.set description value equals retrieved description value');
});

test('duration .get value equals last duration .set value', function () {
    var instance = new st.PracticeSession();
    var original = 654321;
    instance.set('duration', original);
    var retrieved = instance.get('duration');
    equal(original, retrieved, '.set duration value equals retrieved duration value');
});

test('start .get value equals last start .set value', function () {
    var instance = new st.PracticeSession();
    var original = new Date(2014, 1, 12, 5, 2, 3, 123);
    instance.set('start', original);
    var retrieved = instance.get('start');
    equal(original, retrieved, '.set start value equals retrieved start value');
});

test('finish .get value equals last finish .set value', function () {
    var instance = new st.PracticeSession();
    var original = new Date(2014, 1, 12, 5, 2, 3, 123);
    instance.set('finish', original);
    var retrieved = instance.get('finish');
    equal(original, retrieved, '.set finish value equals retrieved finish value');
});

module('PracticeSession Validation');

test('validation handles !start && !durationExists && !finish', function () {
    var start = null;
    var duration = null;
    var finish = null;

    var instance = new st.PracticeSession();
    
    instance.set('start', start);
    instance.set('duration', duration);
    instance.set('finish', finish);

    var actualStart = instance.get('start');
    var actualDuration = instance.get('duration');
    var actualFinish = instance.get('finish');

    var expectedStart = start;
    var expectedDuration = duration;
    var expectedFinish = finish;

    equal(actualStart, expectedStart, 'start -> expected: ' + expectedStart + ', actual: ' + actualStart);
    equal(actualDuration, expectedDuration, 'duration -> expected: ' + expectedDuration + ', actual: ' + actualDuration);
    equal(actualFinish, expectedFinish, 'finish -> expected: ' + expectedFinish + ', actual: ' + actualFinish);
});

test('validation handles !start && !durationExists && finish', function () {
    var start = null;
    var duration = null;
    var finish = new Date();

    var instance = new st.PracticeSession();

    instance.set('start', start);
    instance.set('duration', duration);
    instance.set('finish', finish);

    var actualStart = instance.get('start');
    var actualDuration = instance.get('duration');
    var actualFinish = instance.get('finish');

    var expectedStart = finish;
    var expectedDuration = 0;
    var expectedFinish = finish;

    equal(actualStart.getTime(), expectedStart.getTime(), 'start -> expected: ' + expectedStart + ' >> ' + expectedStart.getTime() + ', actual: ' + actualStart + ' >> ' + actualStart.getTime());
    equal(actualDuration, expectedDuration, 'duration -> expected: ' + expectedDuration + ', actual: ' + actualDuration);
    equal(actualFinish.getTime(), expectedFinish.getTime(), 'finish -> expected: ' + expectedFinish + ' >> ' + expectedFinish.getTime() + ', actual: ' + actualFinish + ' >> ' + actualFinish.getTime());
    ok(actualDuration == actualFinish.getTime() - actualStart.getTime(), 'validated: ' + actualDuration + ' milliseconds = ' + actualFinish + '  -  ' + actualStart);
});

test('validation handles !start && durationExists && !finish', function () {
    var start = null;
    var duration = 300000;
    var finish = null;

    var instance = new st.PracticeSession();

    instance.set('start', start);
    instance.set('duration', duration);
    instance.set('finish', finish);

    var actualStart = instance.get('start');
    var actualDuration = instance.get('duration');
    var actualFinish = instance.get('finish');

    var expectedStart = null;
    var expectedDuration = duration;
    var expectedFinish = null;

    equal(actualStart, expectedStart, 'validation shouldn\'t force a start time');
    equal(actualDuration, expectedDuration, 'duration -> expected: ' + expectedDuration + ', actual: ' + actualDuration);
    equal(actualFinish, expectedFinish, 'validation shouldnt force a finish time');
});

test('validation handles !start && durationExists && finish', function () {
    var start = null;
    var duration = 300000;
    var finish = new Date();

    var instance = new st.PracticeSession();

    instance.set('start', start);
    instance.set('duration', duration);
    instance.set('finish', finish);

    var actualStart = instance.get('start');
    var actualDuration = instance.get('duration');
    var actualFinish = instance.get('finish');

    var expectedStart = new Date(finish.getTime() - duration);
    var expectedDuration = duration;
    var expectedFinish = finish;

    equal(actualStart.getTime(), expectedStart.getTime(), 'start -> expected: ' + expectedStart + ' >> ' + expectedStart.getTime() + ', actual: ' + actualStart + ' >> ' + actualStart.getTime());
    equal(actualDuration, expectedDuration, 'duration -> expected: ' + expectedDuration + ', actual: ' + actualDuration);
    equal(actualFinish.getTime(), expectedFinish.getTime(), 'finish -> expected: ' + expectedFinish + ' >> ' + expectedFinish.getTime() + ', actual: ' + actualFinish + ' >> ' + actualFinish.getTime());
    ok(actualDuration == actualFinish.getTime() - actualStart.getTime(), 'validated: ' + actualDuration + ' milliseconds = ' + actualFinish + ' >> ' + actualFinish.getTime() + '  -  ' + actualStart + ' >> ' + actualStart.getTime());
});

test('validation handles start && !durationExists && !finish', function () {
    var start = new Date();
    var duration = null;
    var finish = null;

    var instance = new st.PracticeSession();

    instance.set('start', start);
    instance.set('duration', duration);
    instance.set('finish', finish);

    var actualStart = instance.get('start');
    var actualDuration = instance.get('duration');
    var actualFinish = instance.get('finish');

    var expectedStart = start;
    var expectedDuration = null;
    var expectedFinish = null;

    equal(actualStart.getTime(), expectedStart.getTime(), 'start -> expected: ' + expectedStart + ' >> ' + expectedStart.getTime() + ', actual: ' + actualStart + ' >> ' + actualStart.getTime());
    equal(actualDuration, expectedDuration, 'duration -> expected: ' + expectedDuration + ', actual: ' + actualDuration);
    equal(actualFinish, expectedFinish, 'finish -> expected: ' + expectedFinish + ', actual: ' + actualFinish);
   });

test('validation handles start && !durationExists && finish', function () {
    var start = new Date();
    var duration = null;
    var finish = new Date(start.getTime() + 500000);

    var instance = new st.PracticeSession();

    instance.set('start', start);
    instance.set('duration', duration);
    instance.set('finish', finish);

    var actualStart = instance.get('start');
    var actualDuration = instance.get('duration');
    var actualFinish = instance.get('finish');

    var expectedStart = start;
    var expectedDuration = finish.getTime() - start.getTime();
    var expectedFinish = finish;

    equal(actualStart.getTime(), expectedStart.getTime(), 'start -> expected: ' + expectedStart + ' >> ' + expectedStart.getTime() + ', actual: ' + actualStart + ' >> ' + actualStart.getTime());
    equal(actualDuration, expectedDuration, 'duration -> expected: ' + expectedDuration + ', actual: ' + actualDuration);
    equal(actualFinish.getTime(), expectedFinish.getTime(), 'finish -> expected: ' + expectedFinish + ' >> ' + expectedFinish.getTime() + ', actual: ' + actualFinish + ' >> ' + actualFinish.getTime());
    ok(actualDuration == actualFinish.getTime() - actualStart.getTime(), 'validated: ' + actualDuration + ' milliseconds = ' + actualFinish + ' >> ' + actualFinish.getTime() + '  -  ' + actualStart + ' >> ' + actualStart.getTime());
});

test('validation handles start && durationExists && !finish', function () {
    var start = new Date();
    var duration = 300000;
    var finish = null;

    var instance = new st.PracticeSession();

    instance.set('start', start);
    instance.set('duration', duration);
    instance.set('finish', finish);

    var actualStart = instance.get('start');
    var actualDuration = instance.get('duration');
    var actualFinish = instance.get('finish');

    var expectedStart = start;
    var expectedDuration = 300000;
    var expectedFinish = null;

    equal(actualStart.getTime(), expectedStart.getTime(), 'start -> expected: ' + expectedStart + ' >> ' + expectedStart.getTime() + ', actual: ' + actualStart + ' >> ' + actualStart.getTime());
    equal(actualDuration, expectedDuration, 'duration -> expected: ' + expectedDuration + ', actual: ' + actualDuration);
    equal(actualFinish, expectedFinish, 'finish -> expected: ' + expectedFinish + ', actual: ' + actualFinish);
   });

test('validation handles !start && durationExists && finish', function () {
    var start = null;
    var duration = 300000;
    var finish = new Date();

    var instance = new st.PracticeSession();

    instance.set('start', start);
    instance.set('duration', duration);
    instance.set('finish', finish);

    var actualStart = instance.get('start');
    var actualDuration = instance.get('duration');
    var actualFinish = instance.get('finish');

    var expectedStart = new Date(finish.getTime() - 300000);
    var expectedDuration = 300000;
    var expectedFinish = finish;

    equal(actualStart.getTime(), expectedStart.getTime(), 'start -> expected: ' + expectedStart + ' >> ' + expectedStart.getTime() + ', actual: ' + actualStart + ' >> ' + actualStart.getTime());
    equal(actualDuration, expectedDuration, 'duration -> expected: ' + expectedDuration + ', actual: ' + actualDuration);
    equal(actualFinish.getTime(), expectedFinish.getTime(), 'finish -> expected: ' + expectedFinish + ' >> ' + expectedFinish.getTime() + ', actual: ' + actualFinish + ' >> ' + actualFinish.getTime());
    ok(actualDuration == actualFinish.getTime() - actualStart.getTime(), 'validated: ' + actualDuration + ' milliseconds = ' + actualFinish + ' >> ' + actualFinish.getTime() + '  -  ' + actualStart + ' >> ' + actualStart.getTime());
});

asyncTest('.save saves to parse.com', function () {
    expect(3);

    var session = new st.PracticeSession({
        username: 'test',
        start: new Date('June 12, 2001'),
        duration: 600000,
        description: 'st.PracticeSession .save test'
    });

    session.save(null, {
        success: function (model, response, options) {
            ok(true, 'save successful');
            ok(model.get('id'), 'model id attribute is set to ' + model.get('id'));
            ok(session.get('id'), 'session id attribute was updated to ' + session.get('id'));
            start();
        }
    });
});

module('PracticeSessionCollection');

test('PracticeSessionCollection exists', function () {
    ok(st.PracticeSessionCollection, 'st.PracticeSessionCollection is defined');
});

test('PracticeSessionCollection instance has fetchByUsername function', function () {
    var instance = new st.PracticeSessionCollection();
    ok(instance.fetchByUsername, 'instance.fetchByUsername is defined');
});

asyncTest('.fetchByUsername success is triggered', function () {
    expect(1);
    var instance = new st.PracticeSessionCollection();
    instance.fetchByUsername('test', {
        success: function (results) {
            ok(results, '.fetchByUsername success fired with results');
            start();
        }
    });
});

asyncTest('.fetchByUsername populates collection', function () {
    expect(1);
    var instance = new st.PracticeSessionCollection();
    instance.fetchByUsername('test', {
        success: function () {
            ok(instance.length > 0, '.fetchByUsername populated collection with ' + instance.length + ' items');
            start();
        }
    });
});

asyncTest('.fetchByUsername triggers collection all event', function () {
    expect(2);
    var instance = new st.PracticeSessionCollection();

    var onTrigger = function () {
        ok('all event was triggered');
        ok(instance.length > 0, 'collection populated with ' + instance.length + ' items');
        start();
    };
    instance.on('all', onTrigger, this);
    instance.fetchByUsername('test');
});


asyncTest('.fetchByUsername populates each invidual model\'s attributes', function () {
    var instance = new st.PracticeSessionCollection();

    var onTrigger = function () {
        var numCorrect = 0;
        for (var i = 0; i < instance.length; i++) {
            if (instance.at(i).get('username') === 'test') {
                numCorrect++;
            }
        }
        ok(numCorrect === instance.length, 'retreived ' + numCorrect + ' sessions with the correct username');
        start();
    };
    instance.on('all', onTrigger, this);
    instance.fetchByUsername('test');
});