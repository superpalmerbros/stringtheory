﻿module('core application');

test('application namespace exists', function () {
    ok(st, 'st is defined');
});

test('application controller exists', function () {
    ok(st.app, 'st.app is defined');
});

test('application user exists', function () {
    ok(st.app.user, 'st.app.user is defined');
});

module('parse.com integration');

test('Parse exists', function () {
    ok(Parse, 'Parse global object is defined')
});


