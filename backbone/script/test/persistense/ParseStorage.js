﻿module('Parse Storage');

test('ParseStorage exists', function () {
    ok(st.ParseStorage, 'st.ParseStorage is defined');
});

test('ParsePracticeSession exists', function () {
    ok(st.ParsePracticeSession, 'st.ParsePracticeSession is defined');
});

test('.savePracticeSession persists to parse.com', function () {
    var instance = new st.ParseStorage();
    ok(instance.savePracticeSession, 'instance.savePracticeSession is defined')
});

test('.savePracticeSession persists to parse.com', function () {
    var storage = new st.ParseStorage();
    var results = storage.savePracticeSession({
        username :'test',
        start: new Date('June 12, 1980'),
        duration: 300000,
        description: 'practice'
    });

    ok(results, 'parse.com save returned');
});

test('.getPracticeSessionsByUsername persists to parse.com', function () {
    var instance = new st.ParseStorage();
    ok(instance.getPracticeSessionsByUsername, 'instance.getPracticeSessionsByUsername is defined')
});

asyncTest('.getPracticeSessionsByUsername retreives from parse.com', function () {
    expect(1);
    var storage = new st.ParseStorage();
    storage.getPracticeSessionsByUsername('test', {
        success: function (results) {
            ok(results, 'parse.com getPracticeSessionsByUsername success fired with results');
            start();
        }
    });
});