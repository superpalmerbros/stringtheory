﻿var spb = spb || {};
spb.util = spb.util || {};

spb.util.loadTemplate = function (template) {
    var $template = $(template);
    var src = $template.data('src');
    var templateHTML = '';

    $.ajax({
        async: false,
        url: src,
        dataType: 'html',
        success: function (data, textStatus, jqXHR) { templateHTML = data},
    });

    return templateHTML;
};